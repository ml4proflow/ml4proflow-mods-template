from setuptools import setup, find_namespace_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

name = "" # todo
version = "0.0.1" # todo

cmdclass = {}

try:
    from sphinx.setup_command import BuildDoc
    cmdclass['build_sphinx'] = BuildDoc
except ImportError:
    print('WARNING: Sphinx not available, not building docs')

setup(
    name=name,
    version=version,
    author="", # todo
    author_email="", # todo
    description="", # todo
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="todo",
    project_urls={
        "Main framework": "todo",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=find_namespace_packages(where="src"),
    namespace_packages=['ml4proflow_mods'],
    package_dir={"": "src"},
    data_files=[], # todo if notebooks should be included, example is provided
    # data_files=[('ml4proflow/notebooks', ['notebooks/NAME_OF_NOTEBOOK.ipynb'])]
    entry_points={
    },
    cmdclass=cmdclass,
    python_requires=">=3.6",
    install_requires=[
        "pandas", # todo
    ],
    extras_require={
        "tests": ["pytest"],
        "docs": ["sphinx", "sphinx-rtd-theme", "m2r2"],
    },
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', version),
            'source_dir': ('setup.py', 'docs/source/'),
            'build_dir': ('setup.py', 'docs/build/')
        }
    },
)
