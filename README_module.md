# ml4proflow-mods-template (ToDo: Change name)

[Insert a short description]

[![Tests Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/tests-badge.svg?job=gen-cov)](#CI)
[![Coverage Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/coverage-badge.svg?job=gen-cov)](#CI)
[![Flake8 Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/flake8-badge.svg?job=gen-cov)](#CI)
[![mypy errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/mypy.svg?job=gen-cov)](#CI)
[![mypy strict errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/mypy_strict.svg?job=gen-cov)](#CI)
------------
# Usage 
To configure the module correctly, add the following module configuration to the configuration file:
```json 
{
    # Update the module configuration

    "key" : "value"
}
```

[Insert description of keys]

[Necessary keys:]
- key1
- key2

# Prerequisites (ToDo: Update the Prerequisites)
- [ml4proflow](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow)

# Installation
Activate your virtual environment, clone this repository and install the package with pip:
```console 
$ pip install .
```

# Contribution
```console 
$ pip install -e .
```
